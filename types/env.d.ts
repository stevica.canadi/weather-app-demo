declare module "@env" {
  export const WEATHER_API_KEY: string;
  export const PEXELS_API_KEY: string;
  export const BASE_WEATHER_URL: string;

  // other ones
}
