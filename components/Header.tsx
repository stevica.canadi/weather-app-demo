import React from "react";
import { View, StyleSheet, ViewProps } from "react-native";

interface Props extends ViewProps {
  title: string;
}

const Header: React.FC<Props> = ({ children, style }) => {
  return <View style={[styles.header, style]}>{children}</View>;
};

const styles = StyleSheet.create({
  header: {
    marginTop: 32,
    paddingLeft: 12,
    paddingRight: 12,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
  },
});

export default Header;
