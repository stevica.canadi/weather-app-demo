import React from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  ScrollView,
} from "react-native";
import useLocationFetch from "../hooks/useLocationFetch";
import { OpenWeatherGeoLocation } from "../types/weather";
import { Ionicons } from "@expo/vector-icons";
import { useRecoilState } from "recoil";

interface LocationSearchProps {
  setToggleSearch: React.Dispatch<React.SetStateAction<boolean>>;
}

const LocationSearch = (props: LocationSearchProps) => {
  const {
    location,
    searchResults,
    storedLocationsState,
    handleChange,
    handleSubmit,
    handleRemove,
    handleSave,
  } = useLocationFetch();

  const { setToggleSearch } = props;
  const [storedLocations] = useRecoilState(storedLocationsState);

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Search for locations: </Text>
      <View style={{ flexDirection: "row" }}>
        <TextInput
          value={location}
          onChangeText={handleChange}
          style={styles.input}
          placeholder="Enter location"
        />
        <TouchableOpacity onPress={handleSubmit} style={styles.button}>
          <Ionicons
            style={styles.iconSearch}
            name={`md-search`}
            size={20}
            color={"#ffffff"}
          />
        </TouchableOpacity>
      </View>
      <ScrollView>
        <View style={styles.resultsContainer}>
          {searchResults && searchResults.length > 0 && (
            <Text style={styles.label}>Found locations: </Text>
          )}

          {searchResults?.map((result: OpenWeatherGeoLocation) => (
            <TouchableOpacity
              onPress={() => {
                handleSave(JSON.stringify(result));
                setToggleSearch(false);
              }}
              key={result.lat + result.lon}
              style={styles.result}
            >
              <Text>
                {result.name}, {result.country}
              </Text>
              <Ionicons
                name="md-add"
                size={16}
                color="black"
                style={styles.addIcon}
              />
            </TouchableOpacity>
          ))}

          {storedLocations && storedLocations.length > 0 && (
            <>
              <Text style={{ ...styles.label, marginTop: 12 }}>
                My locations:
              </Text>
              {storedLocations?.map((item) => {
                const location = JSON.parse(item) as OpenWeatherGeoLocation;
                return (
                  <View
                    key={location.lat + location.lon}
                    style={styles.locationContainer}
                  >
                    <Text style={styles.locationText}>{location.name}</Text>
                    <Ionicons
                      name="md-close"
                      size={16}
                      color="black"
                      onPress={() => {
                        handleRemove(item);
                        setToggleSearch(false);
                      }}
                      style={styles.removeIcon}
                    />
                  </View>
                );
              })}
            </>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    marginHorizontal: 20,
  },
  container: {
    flex: 1,
    padding: 24,
    width: "100%",
  },
  label: {
    color: "gray",
    paddingBottom: 8,
    display: "flex",
  },
  input: {
    padding: 10,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: "gray",
    flex: 1,
  },
  button: {
    backgroundColor: "black",
    padding: 10,
    alignItems: "center",
    marginBottom: 10,
    flexShrink: 0,
    justifyContent: "center",
  },
  iconSearch: {
    color: "white",
    paddingHorizontal: 4,
  },
  resultsContainer: {
    width: "100%",
    marginTop: 10,
  },
  result: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 16,
    paddingVertical: 6,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 5,
  },
  locationContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 16,
    paddingVertical: 6,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
    marginBottom: 10,
    backgroundColor: "#000000",
  },
  locationText: {
    fontSize: 16,
    color: "#ffffff",
  },
  removeIcon: {
    padding: 10,
    color: "#ffffff",
  },
  addIcon: {
    padding: 10,
  },
});

export default LocationSearch;
