import React from "react";
import { StyleSheet, View } from "react-native";
import WeatherInfo from "./WeatherInfo";
import WeatherDetails from "./WeatherDetails";
import PexelsBackgroundImage from "./PexelBackground";
import { OpenWeatherResponse } from "../types/weather";

export default function LocationWeather({ route }) {
  const params = route.params as {
    weather: OpenWeatherResponse;
  };
  const { weather } = params;

  return (
    <View style={styles.container}>
      <PexelsBackgroundImage forecast={weather}>
        <View style={styles.main}>
          <WeatherInfo weather={weather}></WeatherInfo>
          <WeatherDetails weather={weather} units={"metric"} />
        </View>
      </PexelsBackgroundImage>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    justifyContent: "center",
  },
  main: {
    flex: 1,
    justifyContent: "flex-start",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  loader: {
    position: "absolute",
    flex: 1,
    zIndex: 2,
    left: 0,
    right: 0,
    top: 60,
    bottom: 0,
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "transparent",
  },
});
