import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { OpenWeatherResponse } from "../types/weather";
import { SvgUri } from "react-native-svg";

const WeatherInfo = ({ weather }: { weather: OpenWeatherResponse }) => {
  const {
    main: { temp },
    weather: [details],
    name,
  } = weather;

  const { icon, main, description } = details;

  const iconUrl = `https://basmilius.github.io/weather-icons/production/line/openweathermap/${icon}.svg`;

  return (
    <View style={styles.container}>
      <Text style={styles.city}>{name}</Text>
      <SvgUri style={styles.icon} uri={iconUrl} />
      <Text style={styles.temperature}>{Math.round(temp)}°</Text>
      <Text style={styles.description}>{description}</Text>
      <Text style={styles.condition}>{main}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  city: {
    fontSize: 16,
    color: "#ffffff",
    marginBottom: 6,
  },
  icon: {
    width: 110,
    height: 110,
  },
  description: {
    textTransform: "capitalize",
    color: "#ffffff",
  },
  temperature: {
    fontSize: 48,
    color: "#ffffff",
    marginLeft: 15,
    marginBottom: 12,
  },
  condition: {
    fontSize: 20,
    color: "#ffffff",
    fontWeight: "500",
    marginTop: 10,
  },
});

export default WeatherInfo;
