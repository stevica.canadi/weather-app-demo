import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { OpenWeatherResponse } from "../types/weather";

const WeatherDetails = ({
  weather,
  units,
}: {
  weather: OpenWeatherResponse;
  units: string;
}) => {
  const {
    main: { feels_like, humidity, pressure },
    wind: { speed },
  } = weather;

  const windSpeed =
    units === "metric"
      ? `${Math.round(speed)} m/s`
      : `${Math.round(speed)} miles/h`;

  return (
    <View style={styles.container}>
      <View style={styles.column}>
        <View style={styles.row}>
          <MaterialCommunityIcons
            name="thermometer"
            size={16}
            color={"#ffffff"}
          />
          <View style={styles.items}>
            <Text style={styles.textPrimary}>Feels like: </Text>
            <Text style={styles.textSecondary}>{Math.round(feels_like)} °</Text>
          </View>
        </View>
        <View style={styles.row}>
          <MaterialCommunityIcons name="water" size={16} color={"#ffffff"} />
          <View style={styles.items}>
            <Text style={styles.textPrimary}>Humidity: </Text>
            <Text style={styles.textSecondary}>{humidity} %</Text>
          </View>
        </View>
      </View>
      <View style={{ ...styles.column, alignItems: "flex-end" }}>
        <View style={styles.row}>
          <MaterialCommunityIcons
            name="weather-windy"
            size={16}
            color={"#ffffff"}
          />
          <View style={styles.items}>
            <Text style={styles.textPrimary}>Wind Speed: </Text>
            <Text style={styles.textSecondary}>{windSpeed}</Text>
          </View>
        </View>
        <View style={styles.row}>
          <MaterialCommunityIcons
            name="speedometer"
            size={16}
            color={"#ffffff"}
          />
          <View style={styles.items}>
            <Text style={styles.textPrimary}>Pressure: </Text>
            <Text style={styles.textSecondary}>{pressure} hPa</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 16,
    justifyContent: "space-between",
    flexDirection: "row",
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
  column: { flex: 1, paddingHorizontal: 12, paddingBottom: 24, opacity: 0.95 },
  items: {
    flexDirection: "row",
    paddingHorizontal: 4,
    paddingVertical: 6,
  },
  textPrimary: {
    color: "#ffffff",
  },
  textSecondary: {
    color: "#ffffff",
    fontWeight: "700",
  },
});

export default WeatherDetails;
