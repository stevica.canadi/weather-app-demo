import React, { useEffect, useState } from "react";
import { StyleSheet, View, ActivityIndicator, Alert } from "react-native";
import useWeather, { UnitsMetrics } from "./../hooks/useWeather";
import Header from "./Header";
import BottomSheet from "./BottomSheet";
import WeatherInfo from "./WeatherInfo";
import UnitsToggle from "./UnitsToggle";
import SearchToggle from "./SearchToggle";
import WeatherDetails from "./WeatherDetails";
import PexelsBackgroundImage from "./PexelBackground";
import LocationSearch from "./LocationSearch";
import WeatherList from "./WeatherList";

export default function Dashboard({ navigation }) {
  const [units, setUnits] = useState<UnitsMetrics>("metric");
  const [toggleSearch, setToggleSearch] = useState(false);
  const { forecasts, errorMessage, isLoading } = useWeather({
    units: units,
  });

  const handleError = () => {
    if (errorMessage) {
      Alert.alert("Error", errorMessage, [
        {
          text: "Cancel",
          style: "cancel",
        },
        { text: "OK" },
      ]);
    }
  };

  useEffect(() => {
    handleError();
  }, [errorMessage]);

  return (
    <View style={styles.container}>
      {isLoading && (
        <ActivityIndicator
          size="small"
          animating={true}
          hidesWhenStopped={true}
          style={styles.loader}
        />
      )}

      <PexelsBackgroundImage forecast={forecasts[0]}>
        <View style={styles.main}>
          <Header title="Weather">
            <UnitsToggle units={units} setUnits={setUnits} />
            <SearchToggle toggle={toggleSearch} dispatch={setToggleSearch} />
          </Header>
          {forecasts &&
            (forecasts.length === 1 ? (
              <>
                <WeatherInfo weather={forecasts[0]}></WeatherInfo>
                <WeatherDetails weather={forecasts[0]} units={units} />
              </>
            ) : (
              <WeatherList navigation={navigation} forecasts={forecasts} />
            ))}
        </View>
        <BottomSheet present={toggleSearch}>
          <LocationSearch setToggleSearch={setToggleSearch} />
        </BottomSheet>
      </PexelsBackgroundImage>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000000",
    justifyContent: "center",
  },
  main: {
    flex: 1,
    justifyContent: "flex-start",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  loader: {
    position: "absolute",
    flex: 1,
    zIndex: 10,
    left: 0,
    right: 0,
    top: 60,
    bottom: 0,
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "transparent",
  },
});
