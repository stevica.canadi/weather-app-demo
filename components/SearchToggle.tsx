import React from "react";
import { StyleSheet, Pressable, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";

export default function SearchToggle({
  toggle,
  dispatch,
}: {
  toggle: boolean;
  dispatch: React.Dispatch<React.SetStateAction<boolean>>;
}) {
  const handlePress = () => {
    dispatch(!toggle);
  };

  return (
    <TouchableOpacity style={styles.button} onPress={handlePress}>
      <Ionicons name={"ios-search"} size={16} color={"#ffffff"} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 6,
    paddingHorizontal: 12,
    borderRadius: 2,
    elevation: 0,
    color: "#ffffff",
  },
  text: {
    color: "white",
  },
});
