import React from "react";
import {
  ImageBackground,
  StyleSheet,
  ImageBackgroundProps,
} from "react-native";
import useTimeOfDay from "../hooks/useTimeOfDay";
import { OpenWeatherResponse } from "../types/weather";
import usePexelsImage from "../hooks/usePexelsImage";

interface Props extends ImageBackgroundProps {
  forecast: OpenWeatherResponse;
}

const PexelsBackgroundImage: React.FC<Props> = ({ children, forecast }) => {
  if (!forecast) {
    return;
  }

  const currentWeather = forecast && forecast?.weather[0];
  const timeOfDay = useTimeOfDay({ date: new Date() });
  const conditions = currentWeather?.description;
  const location = forecast.name;
  const time_of_day = timeOfDay;

  const imageUrl = usePexelsImage(conditions, location, time_of_day);

  return (
    <ImageBackground
      resizeMode="cover"
      style={{ ...styles.image }}
      source={{ uri: imageUrl }}
    >
      {children}
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  image: {
    flex: 1,
    justifyContent: "center",
  },
});

export default PexelsBackgroundImage;
