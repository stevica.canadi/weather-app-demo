import React from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { OpenWeatherResponse } from "../types/weather";
import { SvgUri } from "react-native-svg";

interface ListItemProps {
  item: OpenWeatherResponse;
  navigation: any;
}

const ListItem = (props: ListItemProps) => {
  const { navigation, item } = props;

  const {
    main: { temp },
    weather: [details],
    name,
  } = item;

  const { icon } = details;
  const iconUrl = `https://basmilius.github.io/weather-icons/production/line/openweathermap/${icon}.svg`;
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate("Location", { weather: item })}
      style={styles.container}
    >
      <View style={styles.cityContainer}>
        <Text style={styles.city}>{name}</Text>
        <SvgUri style={styles.icon} uri={iconUrl} />
      </View>
      <Text style={styles.temperature}>{Math.round(temp)}°</Text>
    </TouchableOpacity>
  );
};

const WeatherList = ({
  forecasts,
  navigation,
}: {
  forecasts: OpenWeatherResponse[];
  navigation: any;
}) => {
  return (
    <FlatList
      keyExtractor={(forecast) => `${forecast.coord.lat + forecast.coord.lon}`}
      style={{ width: "100%", marginTop: 64 }}
      data={forecasts}
      renderItem={({ item }) => {
        return <ListItem navigation={navigation} item={item} />;
      }}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "space-between",
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 24,
    paddingVertical: 12,
  },
  city: {
    fontSize: 24,
    color: "#ffffff",
  },
  cityContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    width: 48,
    height: 48,
    marginLeft: 16,
  },

  temperature: {
    fontSize: 24,
    color: "#ffffff",
    marginLeft: 16,
  },
  condition: {
    fontSize: 20,
    color: "#ffffff",
    fontWeight: "500",
    marginTop: 10,
  },
});

export default WeatherList;
