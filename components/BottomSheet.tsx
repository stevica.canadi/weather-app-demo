import React, { useCallback, useEffect, useMemo, useRef } from "react";
import { View, StyleSheet } from "react-native";
import {
  BottomSheetModal,
  BottomSheetModalProvider,
} from "@gorhom/bottom-sheet";

const BottomSheet = ({
  present,
  children,
}: {
  present: boolean;
  children?: React.ReactNode;
}) => {
  const snapPoints = useMemo(() => ["25%", "85%"], []);

  const ref = useRef<BottomSheetModal>(null);
  const presentBottomSheet = useCallback(() => ref.current?.present(), []);
  const closeBottomSheet = useCallback(() => ref.current?.close(), []);

  useEffect(() => {
    present ? presentBottomSheet() : closeBottomSheet();
  }, [present]);

  return (
    <BottomSheetModalProvider>
      <BottomSheetModal ref={ref} index={1} snapPoints={snapPoints}>
        <View style={styles.contentContainer}>{children}</View>
      </BottomSheetModal>
    </BottomSheetModalProvider>
  );
};

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    alignItems: "center",
  },
});
export default BottomSheet;
