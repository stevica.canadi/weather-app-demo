import React from "react";
import { StyleSheet, TouchableOpacity, Text } from "react-native";
import { UnitsMetrics } from "../hooks/useWeather";

export default function UnitsToggle({
  units,
  setUnits,
}: {
  units: UnitsMetrics;
  setUnits: React.Dispatch<React.SetStateAction<UnitsMetrics>>;
}) {
  const isMetric = units === "metric";

  const handlePress = () => {
    setUnits(isMetric ? "imperial" : "metric");
  };

  return (
    <TouchableOpacity style={styles.button} onPress={handlePress}>
      <Text style={styles.text}>{isMetric ? "C°" : "F°"}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 6,
    paddingHorizontal: 12,
    borderRadius: 2,
    elevation: 0,
    color: "#ffffff",
  },
  text: {
    color: "white",
  },
});
