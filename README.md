# Weather App using React Native and Open Weather Map Api

Simple weather app built using open weather map api and react native. The application gets the location
of the device and displays weather details according to the current device location.
Also it has a search for locations in the top right where users can store their favorite locations.

## Installation

Install EXP following this guide, be sure to have also Android Studio installed and IOS simulator ready

https://docs.expo.dev/get-started/installation/

Inside the project root folder,install packages using the following command

```bash
yarn install
```

## Running the Application

Run the application with the following command

```bash
yarn start
```

## Setting up the .env

Run the application with the following command

Create a .env file with all needed values the example is in the .env-example, simply copy paste it and add API keys needed for the app to work
