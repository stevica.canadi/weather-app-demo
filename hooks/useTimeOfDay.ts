import { useState, useEffect } from "react";

type TimeOfDay =
  | "morning"
  | "before noon"
  | "noon"
  | "afternoon"
  | "evening"
  | "night"
  | "midnight";

interface TimeOfDayHookProps {
  date: Date;
}

const useTimeOfDay = ({ date }: TimeOfDayHookProps): TimeOfDay => {
  const [timeOfDay, setTimeOfDay] = useState<TimeOfDay>("midnight");

  useEffect(() => {
    const currentHour = date.getHours();

    switch (true) {
      case currentHour >= 5 && currentHour < 8:
        setTimeOfDay("morning");
        break;
      case currentHour >= 8 && currentHour < 12:
        setTimeOfDay("before noon");
        break;

      case currentHour >= 12 && currentHour < 13:
        setTimeOfDay("noon");
        break;
      case currentHour >= 13 && currentHour < 17:
        setTimeOfDay("afternoon");
        break;
      case currentHour >= 17 && currentHour < 21:
        setTimeOfDay("evening");
        break;
      case currentHour >= 21 || currentHour < 5:
        setTimeOfDay("night");
        break;
      default:
        setTimeOfDay("midnight");
        break;
    }
  }, [date, setTimeOfDay]);

  return timeOfDay;
};

export default useTimeOfDay;
