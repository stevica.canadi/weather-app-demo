import { useState, useCallback } from "react";
import { useRecoilState, atom, RecoilState } from "recoil";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { WEATHER_API_KEY } from "@env";

interface LocationFetchHook {
  location: string;
  searchResults: any[];
  storedLocationsState: RecoilState<any[]>;
  handleChange: (location: string) => void;
  handleSubmit: () => void;
  handleSave: (location: string) => void;
  handleRemove: (location: string) => void;
}

const key = "@weatherapp:locations";

const preLocations = async () => {
  const locationsData = await AsyncStorage.getItem(key);
  return locationsData;
};

const storedLocationsState = atom({
  key: "storedLocations",
  default: preLocations().then((response) => {
    return JSON.parse(response) || [];
  }),
});

const useLocationFetch = (): LocationFetchHook => {
  const [location, setLocation] = useState("");
  const [storedLocations, setStoredLocations] =
    useRecoilState(storedLocationsState);
  const [searchResults, setSearchResults] = useState([]);

  const fetchSearchResults = useCallback(async () => {
    if (!location) return;
    const apiUrl = `http://api.openweathermap.org/geo/1.0/direct?q=${location}&limit=10&appid=${WEATHER_API_KEY}`;
    try {
      const res = await fetch(apiUrl);
      const data = await res.json();
      setSearchResults(data);
    } catch (error) {
      console.error("Error fetching search results: ", error);
    }
  }, [location]);

  const handleChange = (newLocation: string) => {
    setLocation(newLocation);
  };

  const handleSubmit = useCallback(() => {
    fetchSearchResults();
  }, [location, fetchSearchResults]);

  const handleSave = async (location: string) => {
    try {
      setStoredLocations((prevLocations) => [...prevLocations, location]);
      await AsyncStorage.setItem(key, JSON.stringify(storedLocations));
    } catch (error) {
      console.error(error);
    }
  };

  const handleRemove = async (location: string) => {
    try {
      setStoredLocations((prevLocations) =>
        prevLocations.filter((loc) => loc !== location)
      );
      await AsyncStorage.setItem(key, JSON.stringify(storedLocations));
    } catch (error) {
      console.error(error);
    }
  };

  return {
    location,
    searchResults,
    storedLocationsState,
    handleChange,
    handleSubmit,
    handleSave,
    handleRemove,
  };
};

export default useLocationFetch;
