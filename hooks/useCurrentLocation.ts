import { useState, useEffect } from "react";
import * as Location from "expo-location";
import { Alert } from "react-native";

const useCurrentLocation = () => {
  const [location, setLocation] = useState<{
    lat: number;
    lon: number;
  } | null>();

  useEffect(() => {
    const getLocation = async () => {
      try {
        const { status } = await Location.requestForegroundPermissionsAsync();

        if (status !== "granted") {
          Alert.alert(
            "Insufficient permissions!",
            "Sorry, we need location permissions to make this work!",
            [{ text: "Okay" }]
          );
          return;
        }
        if (status !== "granted") {
          Location.requestForegroundPermissionsAsync();
        }
        const {
          coords: { latitude, longitude },
        } = await Location.getCurrentPositionAsync();

        setLocation({
          lat: latitude,
          lon: longitude,
        });
      } catch (error) {
        console.error(error);
      }
    };

    getLocation();
  }, []);

  return location;
};

export default useCurrentLocation;
