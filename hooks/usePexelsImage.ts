import { useState, useEffect } from "react";
import { PEXELS_API_KEY } from "@env";

export default function usePexelsImage(
  forecast: string,
  location: string,
  time_of_day: string
) {
  const [imageUrl, setImageUrl] = useState<string>();

  useEffect(() => {
    const fetchImage = async () => {
      try {
        const headers = {
          Authorization: `${PEXELS_API_KEY}`,
          "Content-Type": "application/json",
        };
        const params = {
          query: `${forecast}, ${location}, ${time_of_day}, weather`,
          size: "small",
          orientation: "portrait",
        };
        const queryString = Object.keys(params)
          .map((key) => key + "=" + params[key])
          .join("&");

        const response = await fetch(
          `https://api.pexels.com/v1/search?query=${queryString}`,
          { headers }
        );
        if (response.ok) {
          const jsonResponse = await response.json();
          setImageUrl(jsonResponse.photos[1].src.portrait);
        } else {
          setImageUrl(
            "https://images.pexels.com/photos/14206242/pexels-photo-14206242.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750"
          );
        }
      } catch (error) {
        console.error(error);
      }
    };

    fetchImage();
  }, [forecast, location, time_of_day]);

  return imageUrl;
}
