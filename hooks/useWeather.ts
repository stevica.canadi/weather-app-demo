import { useState, useEffect } from "react";
import { WEATHER_API_KEY, BASE_WEATHER_URL } from "@env";
import { OpenWeatherResponse, OpenWeatherGeoLocation } from "../types/weather";
import useCurrentLocation from "./useCurrentLocation";
import useLocationFetch from "./useLocationFetch";
import { useRecoilState } from "recoil";

export type UnitsMetrics = "imperial" | "metric";

interface useWeatherProps {
  units: UnitsMetrics;
}

export default function useWeather(props: useWeatherProps) {
  const { units } = props;
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [forecasts, setForecasts] = useState<OpenWeatherResponse[]>([]);
  const [errorMessage, setErrorMessage] = useState<string>(null);
  const location = useCurrentLocation();
  const [allLocations, setAllLocations] = useState<OpenWeatherGeoLocation[]>(
    []
  );
  const { storedLocationsState } = useLocationFetch();
  const [storedLocations] = useRecoilState(storedLocationsState);

  useEffect(() => {
    if (location) {
      setAllLocations([
        ...storedLocations.map((loc) => JSON.parse(loc)),
        location,
      ]);
    }
  }, [location, storedLocations]);

  const fetchWeather = async () => {
    setIsLoading(true);
    setForecasts([]);
    setErrorMessage(null);

    try {
      const forecastsPromises = allLocations.map(async (location) => {
        if (location) {
          const forecastUrl = `${BASE_WEATHER_URL}lat=${location.lat}&lon=${location.lon}&units=${units}&appid=${WEATHER_API_KEY}`;
          const response = await fetch(forecastUrl);
          const result = await response.json();
          return result;
        }
      });
      const forecasts = await Promise.all(forecastsPromises);
      setForecasts(forecasts);
      setIsLoading(false);
    } catch (error) {
      setErrorMessage(error.message);
    }
  };

  useEffect(() => {
    if (allLocations.length) {
      fetchWeather();
    }
  }, [units, allLocations]);

  return { forecasts, errorMessage, isLoading };
}
