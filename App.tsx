import React from "react";
import { StyleSheet } from "react-native";
import Dashboard from "./components/Dashboard";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import LocationWeather from "./components/LocationWeather";
import { RecoilRoot } from "recoil";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <RecoilRoot>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Dashboard"
            component={Dashboard}
            options={{
              headerStyle: styles.header,
              headerTintColor: "#fff",
            }}
          />
          <Stack.Screen
            name="Location"
            options={{
              headerStyle: styles.header,
              headerTintColor: "#fff",
            }}
          >
            {(props) => {
              return <LocationWeather {...props} />;
            }}
          </Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    </RecoilRoot>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: "#ffffff",
    justifyContent: "center",
    backgroundColor: "#000000",
  },
  header: {
    backgroundColor: "#000000",
  },
});
